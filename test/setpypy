#!/bin/sh
#
# setpypy - create a local link from 'pypy' to a specified version
#
# This script is used to to redirect the 'pypy' in reposurgeon's
# shebang line to a specified version when running regression tests.

pypybin=

make_script() {
	cat >pypy <<-EOF
	#!/bin/sh
	exec "$1" "\$@"
	EOF
	chmod +x pypy
}

find_pypy() {
	pypybin=`command -v pypy`
	test -n "$pypybin" || { echo "setpypy: no pypy binary" >&2 && exit 1; }
}

if [ -z "$1" ]
then
	if [ -e pypy ]
	then
		sed '/^exec /!d; s/^exec "\([^"][^"]*\)" "$@"$/\1/; q' pypy
	else
		find_pypy
		echo $pypybin
	fi
elif [ $1 = "pypy" ]
then
	rm -f ./pypy
	find_pypy
	echo "pypy -> $pypybin"
elif [ $1 = python -o $1 = python2 -o $1 = python3 ]
then
	p=`command -v $1`
	case $p in
		*/bin/*) make_script $p; echo "pypy -> $p";;
		*)
			# Python2 prints version on stderr; Python3 on stdout.
			v=`{ python -V 2>&1; } | sed '/Python [23]\./{y/P/p/;s/ //;s/\..*$//;}'`
			if [ $1 = "$v" ]
			then
				p=`command -v python`
				make_script "$p"
				echo "pypy -> $p"
			else
				echo "setpypy: no python binary" >&2; exit 1
			fi
			;;
	esac
else
	echo "setpypy: unrecognized python version" >&2
	exit 1
fi

exit 0
